var aerialAssault = function()
{
    this.canvas = document.getElementById("aerial-canvas"),
    this.ctx = this.canvas.getContext('2d'),
    this.ctx.fillStyle = "#0000ff",
    this.ctx.strokeStyle =  "‎#0000ff",
    this.CANVAS_WIDTH = 680,
    this.CANVAS_HEIGHT = 300,
    this.EnemyArray = [];
    this.COUNTER = 0;
    this.BulletArray = [];
    this.ExplosionArray =[];
    this.PositionArray = [700,700,760,690,800];
    this.fps = 60;
    this.times = [];
    this.ENEMYSPEED = -2;

    //Booleans
    this.SOUNDON = true;
    this.ISOver = 0;
    this.ISpaused = 0;
    this.HasStarted = false;
    this.Lag = false;

    this.CANVAS_SCORE=0;
    this.HIGHSCORE = 0;

    this.MenuImg = new Image();
    this.MenuImg.src = "./Images/Spitfire.jpg";
    this.SpriteImg = new Image();
    this.SpriteImg.src = "./Images/Spitfire_sprites_transparent.png";
    this.BACKGROUND_IMAGE = new Image();
    this.BACKGROUND_IMAGE_OFFSCREEN = new Image();
    this.BACKGROUND_IMAGE.src ="./Images/background.jpg";
    this.BACKGROUND_IMAGE_OFFSCREEN.src ="./Images/background.jpg";

    this.MenuImg = new Image();
    this.MenuImg.src ="./Images/Spitfire.jpg";

    this.ExplosionImage = new Image();
    this.ExplosionImage.src = "./Images/explosion1.png";

    //Sounds
    this.SHOOTING_SOUND = new Audio("./Sounds/gunshot.wav");
    this.SHOOTING_SOUND.volume = 0.0;
    this.EXPLOSION_SOUND = new Audio("./Sounds/explosionNew.wav");
    this.EXPLOSION_SOUND.volume = 0.1;
    this.BACKGROUND_MUSIC = new Audio("./Sounds/backgroundMusic.mp3");
    this.BACKGROUND_MUSIC.volume = 0.0

    //Background OFFSETS
    this.BACKGROUND_OFFSET_START = 680;
    this.BACKGROUND_IMAGE_OFFSCREEN_OFFSET =680;
    this.BACKGROUND_IMAGE_OFFSET =0;

        ///Arrow Keys
    this.UP_ARROW = 38;
    this.DOWN_ARROW = 40;
    this.PAUSE_KEY = 80;
    this.SPACE_BAR = 32;
    this.RESET = 82;
    this.START = 83;


    //Character Initial Attributes
    this.CHARACTER_HEIGHT = 30;
    this.CHARACHTER_WIDTH = 80;
    this.CHTR_X_POSITION = 30;
    this.CHTR_Y_POSITION = 60;

    //TestSound
    // this.snd1  = document.getElementById("snailbait-audio-sprites");
    // this.src1  = document.createElement("source");
    // this.src1.type = "Sounds/mpeg";
    // this.src1.src  = "Sounds/explosion-012.mp3";
    // this.snd1.appendChild(this.src1);

    // this.snd2  = new Audio();
    // this.src2  = document.createElement("source");
    // this.src2.type = "Sounds/mpeg";
    // this.src2.src  = "Sounds/(TESBIHAT).mp3";
    // this.snd2.appendChild(src2);



    //SpriteSheet


    //explosion Sprite
    this.explosionSprites = [
        { EXPLO_X : 459, EXPLO_Y : 228, EXPLO_WIDTH : 189, EXPLO_HEIGHT : 175},
        { EXPLO_X : 674, EXPLO_Y : 228, EXPLO_WIDTH : 189, EXPLO_HEIGHT : 175},
        { EXPLO_X : 890, EXPLO_Y : 228, EXPLO_WIDTH : 190, EXPLO_HEIGHT : 175},
        { EXPLO_X : 6  , EXPLO_Y : 424, EXPLO_WIDTH : 215, EXPLO_HEIGHT : 203},
        { EXPLO_X : 218, EXPLO_Y : 424, EXPLO_WIDTH : 215, EXPLO_HEIGHT : 203},
        { EXPLO_X : 445, EXPLO_Y : 424, EXPLO_WIDTH : 215, EXPLO_HEIGHT : 203},
        { EXPLO_X : 661, EXPLO_Y : 424, EXPLO_WIDTH : 215, EXPLO_HEIGHT : 203},
        { EXPLO_X : 899, EXPLO_Y : 437, EXPLO_WIDTH : 177, EXPLO_HEIGHT : 174}
    ];

    this.test = [{ EXPLO_X : 459, EXPLO_Y : 228, EXPLO_WIDTH : 189, EXPLO_HEIGHT : 175}];

    //Bullet Sprites
    this.BULLET_RENDER_HEIGHT = 20;
    this.BULLET_RENDER_WIDTH = 30;
    this.BULLETS = [
        {   BULLET_SPRITE_X : 116, BULLET_SPRITE_Y : 241, BULLET_SPRITE_WIDTH : 18, BULLET_SPRITE_HEIGHT : 13,},
        {   BULLET_SPRITE_X : 132,BULLET_SPRITE_Y : 241,BULLET_SPRITE_WIDTH : 18, BULLET_SPRITE_HEIGHT : 13},
        {   BULLET_SPRITE_X : 150, BULLET_SPRITE_Y : 241, BULLET_SPRITE_WIDTH : 20, BULLET_SPRITE_HEIGHT : 13}
    ];

    //This is outdated bullet just leave it in to error check
    this.BULLET_SPRITE_3_X = 150;
    this.BULLET_SPRITE_3_Y = 241;
    this.BULLET_SPRITE_3_WIDTH = 20;
    this.BULLET_SPRITE_3_HEIGHT = 13;


    //CHTR SPRITE IMAGES
    //This could have been put in an array but its too late to change it now
    this.CHTR_SPRITE_X = 46;
    this.CHTR_SPRITE_Y= 236;
    this.CHTR_SPRITE_HEIGHT= 24;
    this.CHTR_SPRITE_WIDTH= 67;

    this.MESSERSCHMITT_X= 270;
    this.MESSERSCHMITT_Y= 244;
    this.MESSERSCHMITT_HEIGHT= 23;
    this.MESSERSCHMITT_WIDTH= 67;
    this.MESSERSCHMITT_CHTR_HEIGHT= 30;
    this.MESSERSCHMITT_CHTR_WIDTH= 80;

    this.MESSERSCHMITT_BF_X= 255;
    this.MESSERSCHMITT_BF_Y= 294;
    this.MESSERSCHMITT_BF_HEIGHT= 29;
    this.MESSERSCHMITT_BF_WIDTH= 95;
    this.MESSERSCHMITT_BF_CHTR_HEIGHT = 40;
    this.MESSERSCHMITT_BF_CHTR_WIDTH = 110;

    this.STUKA_X = 274;
    this.STUKA_Y = 269;
    this.STUKA_HEIGHT = 24;
    this.STUKA_WIDTH = 63;
    this.STUKA_CHTR_HEIGHT = 30;
    this.STUKA_CHTR_WIDTH = 80;

    this.DORNIER_X = 241;
    this.DORNIER_Y = 322;
    this.DORNIER_HEIGHT = 31;
    this.DORNIER_WIDTH = 127;
    this.DORNIER_CHTR_WIDTH = 150;
    this.DORNIER_CHTR_HEIGHT= 40;

    //This creates the player character and puts in their start position
    this.myGamePiece = new gdcharacter(this.CHARACHTER_WIDTH,this.CHARACTER_HEIGHT
        ,this.CHTR_X_POSITION,this.CHTR_Y_POSITION,this.SpriteImg,this.CHTR_SPRITE_X,this.CHTR_SPRITE_Y,
        this.CHTR_SPRITE_WIDTH,this.CHTR_SPRITE_HEIGHT);

    //These are static and used for start menu
    this.dornier = new gdcharacter(this.DORNIER_CHTR_WIDTH,this.DORNIER_CHTR_HEIGHT,500,30
        ,this.SpriteImg,this.DORNIER_X,this.DORNIER_Y,this.DORNIER_WIDTH,this.DORNIER_HEIGHT);
    this.stuka = new gdcharacter(this.STUKA_CHTR_WIDTH,this.STUKA_CHTR_HEIGHT,300,100
        ,this.SpriteImg,this.STUKA_X,this.STUKA_Y,this.STUKA_WIDTH,this.STUKA_HEIGHT);
    this.MessBF = new gdcharacter(this.MESSERSCHMITT_BF_CHTR_WIDTH,this.MESSERSCHMITT_BF_CHTR_HEIGHT,450,160
        ,this.SpriteImg,this.MESSERSCHMITT_BF_X,this.MESSERSCHMITT_BF_Y,this.MESSERSCHMITT_BF_WIDTH,
        this.MESSERSCHMITT_BF_HEIGHT);
    this.Mess = new gdcharacter(this.MESSERSCHMITT_CHTR_WIDTH,this.MESSERSCHMITT_CHTR_HEIGHT,250,240
        ,this.SpriteImg,this.MESSERSCHMITT_X,this.MESSERSCHMITT_Y,this.MESSERSCHMITT_WIDTH,
        this.MESSERSCHMITT_HEIGHT);


    /*This Function increases the difficulty of the game every 5 seconds as thats roughly time it takes enemies to cross the screen
    *It randomises a number of enemies based on how long this game goes on for so the longer the game the moreenemies that will likely appear
    * It also allows for periods of lul between waves as it is random can let you have a break between waves by only sending out 1 or 2
    */
    setInterval(function() {
        if(!AerialAssault.ISpaused && AerialAssault.HasStarted)
        {
            if(AerialAssault.COUNTER === 0)
            {
                console.log("in here")
                AerialAssault.enemyManager(1);
            }
            else
            {
                var z = Math.floor((Math.random() * AerialAssault.COUNTER) + 1);

                console.log("this in here " + AerialAssault.COUNTER);
                AerialAssault.enemyManager(z)
            }
        }
    },5000);
    // setInterval(function()
    // {
    //     AerialAssault.BACKGROUND_MUSIC.play();
    // },244000)
};

aerialAssault.prototype =
{
    /*
     * This Is called when the page loads up
     */
    startGame: function()
    {
        // AerialAssault.BACKGROUND_MUSIC.play();
        // this.ctx.fillRect(30, 10, 30, 120);
        // requestAnimationFrame(this.GameUpdateLoop);
        // AerialAssault.startMenu()
        // this.GameUpdateLoop();
        // AerialAssault.COUNTER = 0;
        // AerialAssault.startMenu();
        // AerialAssault.Explosion.render(AerialAssault.ctx);
        AerialAssault.HasStarted = true;
    },

    startMenu: function()
    {
        AerialAssault.refreshLoop();
        AerialAssault.setHighScoreOnStart();
        AerialAssault.ctx.drawImage(AerialAssault.BACKGROUND_IMAGE,0, 0,AerialAssault.CANVAS_WIDTH,AerialAssault.CANVAS_HEIGHT);
        AerialAssault.ctx.font = "50px Roboto";

        AerialAssault.myGamePiece.Draw(AerialAssault.ctx);
        AerialAssault.dornier.Draw(AerialAssault.ctx);
        AerialAssault.stuka.Draw(AerialAssault.ctx);
        AerialAssault.MessBF.Draw(AerialAssault.ctx);
        AerialAssault.Mess.Draw(AerialAssault.ctx);

        AerialAssault.ctx.fillText(     "Aerial Assault",   185, 80);
        AerialAssault.ctx.strokeText(   "Aerial Assault", 185, 80);

        AerialAssault.ctx.font = "20px Roboto";
        AerialAssault.ctx.fillStyle = "red;"
        AerialAssault.ctx.fillText(     "PRESS S TO START",     250, 180);
        AerialAssault.ctx.strokeText(   "PRESS S TO START",     250, 180);
        if(!AerialAssault.HasStarted)
        {
            requestAnimationFrame(AerialAssault.startMenu);
        }
        else
        {
            AerialAssault.GameUpdateLoop();
        }
    },

    /*
     * This takes in a cookie if it exists and parses it into the highscore
     * Credit To cameron for finally figuring out how it worked i was struggling with this for some time till he helped me
     */
    setHighScoreOnStart: function()
    {
        let cookie =  document.cookie;

        if (cookie === "")
        {
            AerialAssault.HIGHSCORE = 0;
        }
        else
        {
            let split = cookie.split("=");
            let x = split[1];
            AerialAssault.HIGHSCORE = parseInt(x);

            document.getElementById("highScore").innerHTML = "Highscore: "+AerialAssault.HIGHSCORE;
        }

    },

    gameOverMenu: function()
    {
        AerialAssault.ctx.font = "50px Arial";
        AerialAssault.ctx.fillText(     "Game Over",   185, 80);
        AerialAssault.ctx.strokeText(   "Game Over", 185, 80);
    },

    pausedMenu()
    {
        AerialAssault.ctx.font = "50px Roboto";
        AerialAssault.ctx.fillText(     "Game Paused",   185, 80);
        AerialAssault.ctx.strokeText(   "Game Paused", 185, 80);
        if(AerialAssault.Lag === true)
        {
            AerialAssault.ctx.font = "25px Roboto";
            AerialAssault.ctx.fillText(     "Game is lagging", 220, 120);
            AerialAssault.ctx.strokeText(   "Game is lagging", 220, 120);
        }
    },



    checkFPS: function()
    {
      if(AerialAssault.fps < 30)
      {
          AerialAssault.Lag = true;
          AerialAssault.ISpaused = 1;
      }
    },

    /*
     *This is the game update loop it clears the canvas of all items then calls functions that redraws all pieces in their new position
     * It also checks the position of all objects and reports any collision
     * Also writes the Score
     * This will not update the game if the game over or paused flag has been thrown
     */
    GameUpdateLoop : function(now) {
        AerialAssault.refreshLoop();
        if(AerialAssault.ISpaused === 0 && AerialAssault.ISOver ===0)
        {
            AerialAssault.checkFPS();
            AerialAssault.clear();
            AerialAssault.checkCollision();
            AerialAssault.backgroundManager();
            AerialAssault.myGamePiece.Draw(AerialAssault.ctx);
            AerialAssault.enemyUpdate();
            AerialAssault.BulletManager();
            AerialAssault.explosionManager();
            AerialAssault.writeScoreAndFps();
            AerialAssault.checkIfOver();
            AerialAssault.difficultyManager();
        }
        else if( AerialAssault.ISpaused === 1)
        {
            AerialAssault.pausedMenu();
        }
        else if (AerialAssault.ISOver === 1)
        {
            AerialAssault.gameOverMenu();
            AerialAssault.saveScore();
        }
        requestAnimationFrame(AerialAssault.GameUpdateLoop);
    },
    difficultyManager: function()
    {
        if(AerialAssault.COUNTER != 0)
        {
            let x =AerialAssault.COUNTER % 20;
            if(x === 0)
            {
                AerialAssault.ENEMYSPEED -= 2;
            }
        }
    },

    clear : function(){
        this.ctx.clearRect(0, 0, this.CANVAS_WIDTH, this.CANVAS_HEIGHT);
    },

    resetFunction: function()
    {
        var startY = 60;
        AerialAssault.CANVAS_SCORE = 0;
        AerialAssault.EnemyArray = [];
        AerialAssault.BulletArray = [];
        AerialAssault.ExplosionArray = [];
        AerialAssault.COUNTER = 0;
        AerialAssault.ISpaused = 0;
        AerialAssault.ISOver = 0;
        AerialAssault.myGamePiece.setVisible();
    },

    createExplosion: function(x,y)
    {
        AerialAssault.ExplosionArray.push( new AnimatedObject(this.ExplosionImage,
            this.explosionSprites,x,y,60,60));
    },

    /*
     * This advances all the animations of explosions until they are no longer visible
     */
    explosionManager: function()
    {
        for(var i = 0; i < AerialAssault.ExplosionArray.length; i += 1)
        {
            AerialAssault.ExplosionArray[i].render(AerialAssault.ctx);
            AerialAssault.ExplosionArray[i].advance();
        }
    },

    /*
     *This Function is used to create enemies it is called by the set interval function in it it is passed how many enemies need to be spawned
     * the manager recieves that and randomises which type of enemies are to be called as there are four types.
     * The function then pushes each one to the enemy array to be managed by the enemy updater
     */
    enemyManager: function(amount)
    {
        AerialAssault.COUNTER +=1;
        for(var i = 0; i <= amount; i += 1)
        {
            var z = Math.floor((Math.random() * 4) + 1);
            var planeID = Math.floor((Math.random() * 4) + 1);
            var y = Math.floor((Math.random() * 270) + 1);

            if(planeID === 1)
            {
                AerialAssault.EnemyArray.push(new gdcharacter(AerialAssault.MESSERSCHMITT_CHTR_WIDTH,AerialAssault.MESSERSCHMITT_CHTR_HEIGHT,AerialAssault.PositionArray[z],y
                    ,AerialAssault.SpriteImg,AerialAssault.MESSERSCHMITT_X,AerialAssault.MESSERSCHMITT_Y,AerialAssault.MESSERSCHMITT_WIDTH,
                    AerialAssault.MESSERSCHMITT_HEIGHT));
            }
            if(planeID === 2)
            {
                AerialAssault.EnemyArray.push(new gdcharacter(AerialAssault.MESSERSCHMITT_BF_CHTR_WIDTH,AerialAssault.MESSERSCHMITT_BF_CHTR_HEIGHT,AerialAssault.PositionArray[z],y
                    ,AerialAssault.SpriteImg,AerialAssault.MESSERSCHMITT_BF_X,AerialAssault.MESSERSCHMITT_BF_Y,AerialAssault.MESSERSCHMITT_BF_WIDTH,
                    AerialAssault.MESSERSCHMITT_BF_HEIGHT));
            }
            if(planeID ===3)
            {
                AerialAssault.EnemyArray.push(new gdcharacter(AerialAssault.DORNIER_CHTR_WIDTH,AerialAssault.DORNIER_CHTR_HEIGHT,AerialAssault.PositionArray[z],y
                    ,AerialAssault.SpriteImg,AerialAssault.DORNIER_X,AerialAssault.DORNIER_Y,AerialAssault.DORNIER_WIDTH,AerialAssault.DORNIER_HEIGHT));
            }
            if(planeID === 4)
            {
                AerialAssault.EnemyArray.push(new gdcharacter(AerialAssault.STUKA_CHTR_WIDTH,AerialAssault.STUKA_CHTR_HEIGHT,AerialAssault.PositionArray[z],y
                    ,AerialAssault.SpriteImg,AerialAssault.STUKA_X,AerialAssault.STUKA_Y,AerialAssault.STUKA_WIDTH,AerialAssault.STUKA_HEIGHT))
            }
        }
    },

    /*
     * This function updates the enemies by moving them to the left
     */
    enemyUpdate: function()
    {
        for(var i = 0; i < AerialAssault.EnemyArray.length; i += 1)
        {
            AerialAssault.EnemyArray[i].updatePositionX(AerialAssault.ENEMYSPEED);
            AerialAssault.EnemyArray[i].Draw(AerialAssault.ctx);
            // if((AerialAssault.EnemyArray[i].returnPositionX() + AerialAssault.EnemyArray[i].returnWidth()) < 0)
            // {
            //     AerialAssault.EnemyArray.splice(0, i);
            // }
        }
    },

    /*
     * This function checks for collision between bullets and enemies and between enemies and the player
     * Each object has a built in checkCollision function that needs another object passed into it to throw a crash flag
     * this function checks a player between all enemies and all bullets between all enemies onscreen that are visible
     */
    checkCollision: function()
    {
        for(var i = 0; i < AerialAssault.EnemyArray.length; i += 1)
        {
            if (AerialAssault.myGamePiece.checkCrash(AerialAssault.EnemyArray[i])) {
                AerialAssault.EnemyArray[i].Draw(AerialAssault.ctx);
                AerialAssault.createExplosion(AerialAssault.myGamePiece.x + 10,AerialAssault.myGamePiece.y -10);
                AerialAssault.myGamePiece.setInVisible();
                console.log("Ive Been Called- Its Over Jim");
                AerialAssault.ISOver = 1;
            }
            if(AerialAssault.BulletArray.length > 0)
            {
                for(var z = 0; z < AerialAssault.BulletArray.length; z += 1)
                {
                    if(AerialAssault.BulletArray[z].getVisible())
                    {
                        if(AerialAssault.BulletArray[z].checkCrash(AerialAssault.EnemyArray[i]))
                        {
                            AerialAssault.BulletArray[z].setInVisible();
                            AerialAssault.EnemyArray[i].setInVisible();
                            AerialAssault.CANVAS_SCORE += 1;
                            AerialAssault.createExplosion(AerialAssault.EnemyArray[i].x,AerialAssault.EnemyArray[i].y);
                            // AerialAssault.EXPLOSION_SOUND.cloneNode(true).play();
                            AerialAssault.playSound(AerialAssault.EXPLOSION_SOUND);
                        }
                    }

                }
            }

        }

    },

    /*
     * If Any enemy ship reaches the other side of the screen the game is over and this is thrown
     */
    checkIfOver: function()
    {
        for(var i = 0; i < AerialAssault.EnemyArray.length; i += 1)
        {
           if((AerialAssault.EnemyArray[i].returnPositionX() <= 0) && (AerialAssault.EnemyArray[i].getVisible() === true))
           {
               AerialAssault.ISOver = 1;
           }
        }
    },

    checkIfPlayerWantsSound: function()
    {
        AerialAssault.SOUNDON = document.getElementById('sound-checkbox').checked;
    },
    playSound: function(soundToPlay)
    {
        if(AerialAssault.SOUNDON === true)
        {
            soundToPlay.cloneNode(true).play();
        }
    },
    /*
     * This Function manages the background it increments the two background images one off screen to the left and once one is full offscreen it
     * resets it to back to the righthand side
     */
    backgroundManager: function()
    {
        AerialAssault.BACKGROUND_IMAGE_OFFSET +=-1;
        AerialAssault.BACKGROUND_IMAGE_OFFSCREEN_OFFSET += -1;
        AerialAssault.ctx.drawImage(AerialAssault.BACKGROUND_IMAGE,this.BACKGROUND_IMAGE_OFFSET,
            0,AerialAssault.CANVAS_WIDTH,AerialAssault.CANVAS_HEIGHT);
        AerialAssault.ctx.drawImage(AerialAssault.BACKGROUND_IMAGE_OFFSCREEN, this.BACKGROUND_IMAGE_OFFSCREEN_OFFSET,
            0,AerialAssault.CANVAS_WIDTH,AerialAssault.CANVAS_HEIGHT);

        if(AerialAssault.BACKGROUND_IMAGE_OFFSET <= -680)
        {
            AerialAssault.BACKGROUND_IMAGE_OFFSET = AerialAssault.BACKGROUND_OFFSET_START;
        }
        if(AerialAssault.BACKGROUND_IMAGE_OFFSCREEN_OFFSET <= -680)
        {
            AerialAssault.BACKGROUND_IMAGE_OFFSCREEN_OFFSET = AerialAssault.BACKGROUND_OFFSET_START;
        }
    },

    /*
     *This Function is called to simply write te score to the div with the id score if the score is higher than the highscore it modifies the highscore
     */
    writeScoreAndFps: function()
    {
        if(AerialAssault.CANVAS_SCORE > AerialAssault.HIGHSCORE)
        {
            AerialAssault.HIGHSCORE = AerialAssault.CANVAS_SCORE;
            document.getElementById("highScore").innerHTML = "Highscore: "+AerialAssault.CANVAS_SCORE;
        }
        document.getElementById("fps").innerHTML = "FPS: "+AerialAssault.fps;
        document.getElementById("score").innerHTML = "Score: "+AerialAssault.CANVAS_SCORE;
    },

    /*
     * This manages the bullets that are created by the player it increments through the bullet array updates their position by moving them to thr right
     * it then renders the bullet the calls the advance function tp move the bullet onto the next sprite
     */
    BulletManager: function()
    {
        for(let i = 0; i < AerialAssault.BulletArray.length; i += 1)
        {
            AerialAssault.BulletArray[i].updatePosX(2);
            AerialAssault.BulletArray[i].render(AerialAssault.ctx);
            AerialAssault.BulletArray[i].advance();
        }
    },
    /*
     * This allows the player to move up
     */
    MoveUP : function()
    {
        if(AerialAssault.ISpaused === 0) {
            AerialAssault.myGamePiece.updatePositionY(-6);
        }
    },
    /*
     * This allows the player to move down
     */
    MoveDOWN : function()
    {
        if(AerialAssault.ISpaused === 0) {
            AerialAssault.myGamePiece.updatePositionY(6)
        }
    },

    /*
     * TThis creates a new bullet and places it in the bullet array
     */
    CreateBullet: function()
    {
        if(AerialAssault.ISpaused === 0 && AerialAssault.HasStarted === true) {
            // AerialAssault.BulletArray.push(new gdcharacter(AerialAssault.BULLET_RENDER_WIDTH,
            //     AerialAssault.BULLET_RENDER_HEIGHT, ((AerialAssault.myGamePiece.returnPositionX() + AerialAssault.myGamePiece.returnWidth()) + 1), (AerialAssault.myGamePiece.returnPositionY() + 5),
            //     this.SpriteImg, this.BULLET_SPRITE_3_X,
            //     this.BULLET_SPRITE_3_Y, this.BULLET_SPRITE_3_WIDTH, this.BULLET_SPRITE_3_HEIGHT));
            AerialAssault.BulletArray.push(new AnimatedObjectWithoutStop(
                AerialAssault.SpriteImg,AerialAssault.BULLETS,
                (AerialAssault.myGamePiece.returnPositionX() + AerialAssault.myGamePiece.returnWidth() + 1),
                (AerialAssault.myGamePiece.returnPositionY() + 5),AerialAssault.BULLET_RENDER_WIDTH,AerialAssault.BULLET_RENDER_HEIGHT));
            AerialAssault.playSound(AerialAssault.SHOOTING_SOUND);
            // AerialAssault.SHOOTING_SOUND.cloneNode(true).play();
        }
    },
    /*
     *This function pauses the game if the game is already paused it unpauses it
     * This function does not work if the game is over
     */
    pause: function()
    {
        if(AerialAssault.ISpaused === 0)
        {
            AerialAssault.ISpaused = 1;
        }
        else if(AerialAssault.ISpaused === 1)
        {
            AerialAssault.ISpaused = 0;
        }
        console.log(AerialAssault.ISpaused);
    },
    saveScore: function()
    {
      document.cookie = "high_SCORE=" +AerialAssault.HIGHSCORE;
      // localStorage.setItem("high_SCORE=",AerialAssault.HIGHSCORE);
    },

    //This was modified from https://www.growingwiththeweb.com/2017/12/fast-simple-js-fps-counter.html
   refreshLoop: function()
   {
        const now = performance.now();
        while (AerialAssault.times.length > 0 && AerialAssault.times[0] <= now - 1000) {
            AerialAssault.times.shift();
        }
        AerialAssault.times.push(now);
        AerialAssault.fps = AerialAssault.times.length;
    }

};

window.addEventListener(
    'blur',

    function (e) {
        if (AerialAssault.HasStarted)
        {
            AerialAssault.pause();
        }
    }
);


window.addEventListener('keydown', function(e){
    var key = e.keyCode;

    if(key === AerialAssault.UP_ARROW){
        AerialAssault.MoveUP();
    }
    else if(key === AerialAssault.DOWN_ARROW){
        AerialAssault.MoveDOWN();
    }
    else if(key === AerialAssault.PAUSE_KEY)
    {
        if(AerialAssault.ISOver != 1)
        {
            AerialAssault.pause();
        }

    }
    else if(key === AerialAssault.RESET)
    {
        AerialAssault.resetFunction();
    }
    if(key === AerialAssault.SPACE_BAR)
    {
        AerialAssault.CreateBullet();
    }
    if(key === AerialAssault.START)
    {
        AerialAssault.startGame();
    }
});

//
// window.addEventListener('click', function (e) {
//     var canvas = document.getElementById('game-canvas');
//     var context = canvas.getContext('2d');
//
//     console.log(e);
//     //check if mouse is greater than 200px on X&Y axis
//     if (e.clientX <= 200 && e.clientY <= 200) {
//         console.log('true');
//     }
//     //alerts with mouse position
//     if (e.which === 1) {
//         let canvasBoundingRectangle = canvas.getBoundingClientRect();
//         mouseX = e.clientX - canvasBoundingRectangle.left;
//         mouseY = e.clientY - canvasBoundingRectangle.top;
//
//         alert("x:" + mouseY + "     Y:" + mouseY);
//     }
// });

var AerialAssault = new aerialAssault();
AerialAssault.startMenu();

// AerialAssault.BACKGROUND_MUSIC.play();
